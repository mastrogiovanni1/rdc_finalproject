cmake_minimum_required (VERSION 2.8.11)
	

project (mailbox)

add_library(utils SHARED
	utils.c	
)
  
add_executable(server
	server.c
	database.c
)

target_link_libraries(server
	utils
)

add_executable(client
	client.c
)

target_link_libraries(client
	utils
)
  
include_directories(${PROJECT_SOURCE_DIR})

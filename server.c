#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>
#include <errno.h>
#include <pthread.h>
#include <signal.h>

#include "database.h"
#include "utils.h"

typedef struct thread_args{
  int socket;
  struct sockaddr_in addr;
} thread_args;

size_t message_size_no_padding;//all message in the protocol must have this size (plus mail size eventually)
Database mailbox; //mails in the system
Database users; //logged users
int stop_sig;
const char* path;

void int_handler(int sig){
  printf("\nexiting with grace\n");
  database_destroy(&mailbox, destroy_entry, path);
  database_destroy(&users, destroy_user, path);
  stop_sig = 1;
}

void receive_message_client(int sock, Message* m){
  int ret;
  
  ret = recv(sock, (void*)m, message_size_no_padding, 0);
  if(ret == -1){
    exit_with_error(NULL, "error: recv()\n");
  }
  
}

void send_message_client(int sock, Message* m){
  int ret;
  size_t size = strlen(m -> payload);
  size += message_size_no_padding;
  
  ret = send(sock, (void*)m, size, 0);
  if(ret == -1){
    exit_with_error(NULL,"error: send()\n");
  }
  
  free(m);
}

Message* elaborate_message(int sock, Message* m){
  int ret;
  
  //responding to login request
  if(m -> ins == LOGIN){
  
    Message* response = calloc(1, sizeof(Message));
    if(response == NULL){
      exit_with_error(NULL, "error: calloc()");
    }
    
    //check if user is already logged
    if(database_get_by_name(&users, m -> recipient) == NULL){
      database_append_name(&users, m -> recipient);
    }
    else{
      response -> ins = ERROR;
      response -> length = NOT_ALLOWED;
      return response;
    }
    
    //respond OK
    response -> ins = OK;
    return response;
  }
  
  //responding to send request
  else if(m -> ins == SEND){
    int left = m -> length;
    char* mail = malloc(left*sizeof(char));
    
    ret = recv(sock, (void*)mail, left*sizeof(char), 0);
    if(ret == -1){
      exit_with_error(NULL, "error: recv()\n");
    }
    
    //extract recipient, put in database;
    if(database_append(&mailbox, mail, m -> sender, m -> recipient) == NULL){
      exit_with_error(NULL, "error: database_append()");
    }
    
    free(mail);
    
    //respond OK
    Message* response = calloc(1, sizeof(Message));
    if(response == NULL){
      exit_with_error(NULL, "error: calloc()");
    }
    
    response -> ins = OK;
    return response;
  }
  
  //responding to list request
  else if(m -> ins == LIST){

    Message* response = calloc(1, sizeof(Message));
    if(response == NULL){
      exit_with_error(NULL, "error: calloc()");
    }
    
    //look for mails addressed to recipient in database
    Node* first = database_get_by_recipient(&mailbox, m -> recipient);   
    if(first == NULL){
        response -> ins = ERROR;
        response -> length = NOTHING_FOUND;
        return response;      
    }
    
    Node* current = first;
    //respond LISTING for every mail found
    do{
      response -> ins = LISTING;
      Entry* e = (Entry*)current -> entry;
      response -> length = e -> code;
      memcpy(response -> sender, e -> sender, strlen(e -> sender) + 1);
      memcpy(response -> recipient, e -> recipient, strlen(e -> recipient) + 1);
      send_message_client(sock, response);
      
      response = calloc(1, sizeof(Message));
      if(response == NULL){
        exit_with_error(NULL, "error: calloc()");
      }
      
      current = database_get_by_recipient(&mailbox, m -> recipient);
    }while(current != first);
 
    //respond OK
    response -> ins = OK;
    return response;
  }
  
  //responding to read request
  else if(m -> ins == READ){
    int mail_id = m -> length;
    
    //look for mail_id in datbase
    Node* n = database_get_by_code(&mailbox, mail_id); 
    Message* response;
    //if not exists or not for them respond ERROR
    if(n == NULL){
      response = calloc(1, sizeof(Message));
  
      if(response == NULL){
        exit_with_error(NULL, "error: calloc()");
      }
        
      response -> ins = ERROR;
      response -> length = NOTHING_FOUND;
      return response;
    }
    
    Entry* e = (Entry*)n -> entry;
    if(strcmp(m -> recipient, e-> recipient)){
      response = calloc(1, sizeof(Message));
  
      if(response == NULL){
        exit_with_error(NULL, "error: calloc()");
      }
        
      response -> ins = ERROR;
      response -> length = NOT_ALLOWED;
      return response;
    }
    
    e -> read = 1;
    //if found respond SHOWING
    char* mail = e -> mail;
    char* recipient = e -> recipient;
    char* sender = e -> sender;
    
    //put response in payload
    size_t size = message_size_no_padding + strlen(mail) + 1;
    response = calloc(1, size);
    response -> ins = SHOWING;
    response -> length = size - message_size_no_padding;
    memcpy(response -> recipient, recipient, strlen(recipient) + 1);
    memcpy(response -> sender, sender, strlen(sender) + 1);
    response -> payload[0] = '$';
    memcpy(response -> payload + 1, mail, response -> length);
    return response; 
  }
  
  //responding to delete request
  else if(m -> ins == DELETE){
    int mail_id = m -> length;

    Message* response = calloc(1, sizeof(Message));
    if(response == NULL){
      exit_with_error(NULL, "error: calloc()");
    }
    
    //look for mail_id in database    
    Node* n = database_get_by_code(&mailbox, mail_id);
    //if not exists or not for them or not read respond ERROR      
    if(n == NULL){
      response -> ins = ERROR;
      response -> length = NOTHING_FOUND;
      return response;
    }
    
    Entry* e = (Entry*)n -> entry;

    if(strcmp(m -> recipient, e -> recipient)){
      response -> ins = ERROR;
      response -> length = NOT_ALLOWED;
      return response;
    }
    else if(e -> read == 0){
      response -> ins = ERROR;
      response -> length = GENERIC_ERROR;
      return response;    
    }
    
    n = database_remove(&mailbox, n);
    destroy_entry((void*)e, NULL);
    free(n);
    //if found respond OK   
    response -> ins = OK;
    return response;
  }
  
  //responding to quit request
  else if(m -> ins == QUIT){    
    //respond OK
    Message* response = calloc(1, sizeof(Message));
    if(response == NULL){
      exit_with_error(NULL, "error: calloc()");
    }
    
    //remove user who logged out
    Node* n = database_remove_by_name(&users, m -> recipient);
    if(n != NULL){
      destroy_user(n -> entry, path);
      free(n);
    }
    
    response -> ins = OK;
    //close connection
    response -> length = QUIT_CODE;
    return response;
  }
  
  //responding to wrong requests
  else{
    //respond ERROR
    Message* response = calloc(1, sizeof(Message));
    if(response == NULL){
      exit_with_error(NULL, "error: calloc()");
    }
    
    response -> ins = ERROR;
    response -> length = WRONG_FORMAT;
    return response;
  }
}

void* connection_routine(void* t_args){
  thread_args* args = (thread_args*) t_args;
  int sock = args -> socket;
  Message message = {0};
  int over = 0;
  
  while(!over){
    //receive
    receive_message_client(sock, &message);
    //elaborate
    Message* response = elaborate_message(sock, &message);
    
    //check if need to quit
    if(response -> length == QUIT_CODE){
      over = 1;
      response -> length = 0;
    }
    
    //respond
    send_message_client(sock, response);
    memset(&message, 0, sizeof(Message));

    printf("--------------------Mailbox Status--------------------\n");
    database_print(&mailbox, print_entry);
    printf("--------------------Connected Users--------------------\n");
    database_print(&users, print_user);
    
  }
  
  close(sock);
  printf("connection closed\n");
  pthread_exit(NULL); 
}

int main(int argc, const char * argv[]) { 
    if(argc < 2){
      printf("server needs a directory path to store database\n");
      return -1;
    }
    
    path = argv[1];
    stop_sig = 0;
    struct sigaction action = {0};
    action.sa_handler = int_handler;
    sigaction(SIGINT, &action, NULL);
    
    message_size_no_padding = sizeof(Instruction) + sizeof(int) + 2*((MAX_USERNAME_SIZE + 1) * sizeof(char)) + sizeof(char);
    int ret;
    int yes = 1;    
    int client_socket;
    int server_socket;

    database_init(&mailbox, path);
    database_init(&users, NULL);
    
    struct sockaddr_in client_address;
    struct sockaddr_in server_address = {0};
    server_address.sin_family = AF_INET;
    server_address.sin_addr.s_addr = htons(INADDR_ANY);
    server_address.sin_port = htons(PORT);
    socklen_t addr_len = sizeof(struct sockaddr_in);
    
    server_socket = socket(AF_INET, SOCK_STREAM, 0);
    if(0 > server_socket){
      exit_with_error(NULL, "error: socket()\n");
    }
    
    ret = setsockopt(server_socket, SOL_SOCKET, SO_REUSEADDR, (void*)&yes, sizeof(yes));
    if(0 > ret){
      exit_with_error(NULL, "error: setsockopt()\n");
    }
    
    ret = bind(server_socket, (struct sockaddr*)&server_address, addr_len);
    if(0 > ret){
      exit_with_error(NULL, "error: bind()\n");
    }
    
    ret = listen(server_socket, QUEUE_SIZE);
    if(0 > ret){
      exit_with_error(NULL, "error: listen()\n");
    }    
    
    while(!stop_sig){
    
      client_socket = accept(server_socket, (struct sockaddr*)&client_address, &addr_len);
      if(0 > client_socket){
        if(errno == EINTR) continue;
        exit_with_error(NULL, "error: accept()\n");
      }
      
      pthread_t thread;
      thread_args args = {.socket = client_socket, .addr = client_address};
      
      ret = pthread_create(&thread, NULL, connection_routine, (void*)&args);
      if(ret){
        exit_with_error(&ret, "error: pthread_create\n");
      }
      
      ret = pthread_detach(thread);
      if(ret){
        exit_with_error(&ret, "error: pthread_detach\n");
      }
      
    }
    
    close(server_socket);
}

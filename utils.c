#include <errno.h>
#include <stdlib.h>
#include <stdio.h>

#include "utils.h"

void exit_with_error(int* err, const char * msg){
    if(err){
      errno = *err;
    }
    perror(msg);
    exit(EXIT_FAILURE);
}

//creates a unique code for every mail//NOT CURRENTLY USED
int hash_to_code(const char* mail, const char* sender, const char* recipient){
  const char* aux = mail;
  int res = 0;
  while(*aux){
    res += (int)*aux;
    aux++;
  }
  
  aux = sender;
  while(*aux){
    res += (int)*aux;
    aux++;
  }
  
  aux = recipient;
  while(*aux){
    res -= (int)*aux;
    aux++;
  }
  
  return res;
}

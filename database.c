#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <unistd.h>
#include <stdio.h>
#include <errno.h>
#include <dirent.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/stat.h>

#include "utils.h"
#include "database.h"

void database_init(Database* database, const char* path){
  if(database == NULL){
    return;
  }
  
  int ret;
  database -> head = NULL;
  database -> tail = NULL;
  database -> size = 0;
  database -> r_counter = 0;
  database -> last_id = 0;
  
  ret = sem_init(&(database -> w_sem), 0, 1);
  if(-1 == ret){
    exit_with_error(NULL, "error: sem_init()\n");
  }
  
  ret = pthread_mutex_init(&(database -> counter_lock), NULL);
  if(ret){
    exit_with_error(&ret, "error: mutex_init()\n");
  }
  
  ret = pthread_mutex_init(&(database -> counter_id), NULL);
  if(ret){
    exit_with_error(&ret, "error: mutex_init()\n");
  }
    
  if(path != NULL){
    mail_database_read_from_disc(database, path);
  }
}

void entry_init(Database* database, Entry* entry, const char* mail, const char* sender, const char* recipient, int code, int read){
  if(entry == NULL){
    return;
  }

  int ret = 0;

  size_t size_m = strlen(mail) + 1;
  size_t size_s = strlen(sender) + 1;
  size_t size_r = strlen(recipient) + 1;
  
  entry -> mail = malloc(size_m * sizeof(char));
   
  if(code == -1){
    ret = pthread_mutex_lock(&(database -> counter_id));
    if(ret){
      exit_with_error(&ret, "error: lock()\n");
    }
  
    entry -> code = database -> last_id;
    database -> last_id++;
 
    ret = pthread_mutex_unlock(&(database -> counter_id));
    if(ret){
      exit_with_error(&ret, "error: lock()\n");
    }

  }else{
    entry -> code = code;
  }
  
  entry -> read = read;
  
  memcpy(entry -> sender, sender, size_s);
  memcpy(entry -> recipient, recipient, size_r);
  memcpy(entry -> mail, mail, size_m);
  
}

Node* database_append_node(Database* database, Node* n){
  if(database == NULL || n == NULL){
    return NULL;
  }  

  int ret = sem_wait(&(database -> w_sem));
  if(-1 == ret){
    exit_with_error(NULL, "error: sem_wait()\n");
  }

  //CS
  n -> next = NULL;
  n -> prev = NULL;
  
  if(database -> size == 0){
    database -> head = n;
    database -> tail = database -> head;
  }
  else{
    database -> tail -> next = n;
    n -> prev = database -> tail;
    database -> tail = n;
  }
  
  database -> size++;
  //no CS

  ret = sem_post(&(database -> w_sem));
  if(-1 == ret){
    exit_with_error(NULL, "error: sem_wait()\n");
  }

  return n;
}

Node* database_append(Database* database, const char* mail, const char* sender, const char* recipient){    
  if(database == NULL || mail == NULL || sender == NULL || recipient == NULL){
    return NULL;
  }

  Node* new_n = calloc(1,sizeof(Node));
  if(new_n == NULL){
    exit_with_error(NULL, "error: calloc()\n");
  }
  
  new_n -> next = NULL;
  new_n -> prev = NULL;
  
  new_n -> entry = (void*)malloc(sizeof(Entry));
  if(new_n -> entry == NULL){
    exit_with_error(NULL, "error: malloc()\n");
  }
  
  Entry* new_e = (Entry*) new_n -> entry;
  entry_init(database, new_e, mail, sender, recipient, -1, 0);  
    
  return database_append_node(database, new_n);
}

Node* database_pop(Database* database){
  if(database == NULL || database -> size == 0){
    return NULL;
  }

  return database_remove(database, database -> tail); 
}

Node* database_append_name(Database* database, const char* user_name){
  if(database == NULL || user_name == NULL){
    return NULL;
  }
  
  Node* new_n = calloc(1,sizeof(Node));
  if(new_n == NULL){
    exit_with_error(NULL, "error: calloc()\n");
  }
  
  new_n -> next = NULL;
  new_n -> prev = NULL;
  
  new_n -> entry = (void*)calloc(1,sizeof(User));
  if(new_n -> entry == NULL){
    exit_with_error(NULL, "error: calloc()\n");
  }
  
  User* new_u = (User*) new_n -> entry;
  memcpy(new_u -> user_name, user_name, strlen(user_name) + 1);  
    
  return database_append_node(database, new_n);
}

Node* database_get_by_code(Database* database, int code){
  if(database == NULL || database -> size == 0){
    return NULL;
  }
  
  int ret;
  ret = pthread_mutex_lock(&(database -> counter_lock));
  if(ret){
    exit_with_error(&ret, "error: lock()\n");
  }
  database -> r_counter++;
  if(database -> r_counter == 1){
      ret = sem_wait(&(database -> w_sem));
      if(-1 == ret){
        exit_with_error(NULL, "error: sem_wait()\n");
      }
  } 
  ret = pthread_mutex_unlock(&(database -> counter_lock));
  if(ret){
    exit_with_error(&ret, "error: unlock()\n");
  }  
  //
  Node* current = database -> head;
  Entry* e = (Entry*) current -> entry;
  while(e -> code != code){

    current = current -> next;
    if(current == NULL){
      break;
    }
    e = (Entry*) current -> entry;
  }
  //
  ret = pthread_mutex_lock(&(database -> counter_lock));
  if(ret){
    exit_with_error(&ret, "error: lock()\n");
  }
  database -> r_counter--;
  if(database -> r_counter == 0){
      ret = sem_post(&(database -> w_sem));
      if(-1 == ret){
        exit_with_error(NULL, "error: sem_post()\n");
      }
  } 
  ret = pthread_mutex_unlock(&(database -> counter_lock));
  if(ret){
    exit_with_error(&ret, "error: unlock()\n");
  }   

  return current;
}

Node* database_get_by_name(Database* database, const char* user_name){
  if(database == NULL || database -> size == 0){
    return NULL;
  }
  
  int ret = pthread_mutex_lock(&(database -> counter_lock));
  if(ret){
    exit_with_error(&ret, "error: lock()\n");
  }
  database -> r_counter++;
  if(database -> r_counter == 1){
      ret = sem_wait(&(database -> w_sem));
      if(-1 == ret){
        exit_with_error(NULL, "error: sem_wait()\n");
      }
  } 
  ret = pthread_mutex_unlock(&(database -> counter_lock));
  if(ret){
    exit_with_error(&ret, "error: unlock()\n");
  } 
  
  //
  Node* current = database -> head;
  User* u = (User*) current -> entry;
  while(strcmp(u -> user_name, user_name)){

    current = current -> next;
    if(current == NULL){
      break;
    }
    
    u = (User*) current -> entry;
  }
  //
  
  ret = pthread_mutex_lock(&(database -> counter_lock));
  if(ret){
    exit_with_error(&ret, "error: lock()\n");
  }
  database -> r_counter--;
  if(database -> r_counter == 0){
      ret = sem_post(&(database -> w_sem));
      if(-1 == ret){
        exit_with_error(NULL, "error: sem_post()\n");
      }
  } 
  ret = pthread_mutex_unlock(&(database -> counter_lock));
  if(ret){
    exit_with_error(&ret, "error: unlock()\n");
  }   

  return current;
}


Node* database_remove(Database* database, Node* n){
  if(database == NULL || n == NULL){
    return NULL;
  }
  
  int ret;
  ret = sem_wait(&(database -> w_sem));
  if(-1 == ret){
    exit_with_error(NULL, "error: sem_wait()\n");
  }
  
  //CS
  else{
    Node* prev = n -> prev;
    Node* next = n -> next;
    if(prev != NULL){
      prev -> next = next;
    }
    if(next != NULL){
      next -> prev = prev;
    }
    if(n == database -> head){
      database -> head = next;
    }
    if(n == database -> tail){
      database -> tail = prev;
    }
  }
  
  database -> size--;
  //no CS
  
  ret = sem_post(&(database -> w_sem));
  if(-1 == ret){
    exit_with_error(NULL, "error: sem_wait()\n");
  }
  
  return n;
}

Node* database_remove_by_code(Database* database, int code){
  if(database == NULL || database -> size == 0){
    return NULL;
  }
  
  Node* rem = database_get_by_code(database, code);
  if(rem == NULL){
    return NULL;
  }
  
  return database_remove(database, rem);
}

Node* database_remove_by_name(Database* database, const char* user_name){
  if(database == NULL || database -> size == 0){
    return NULL;
  }
  
  Node* rem = database_get_by_name(database, user_name);
  if(rem == NULL){
    return NULL;
  }
  
  return database_remove(database, rem); 
}

Node* database_get_by_recipient(Database* database, const char* recipient){
  if(database == NULL || database -> size == 0 ){
    return NULL;
  }
 
  int ret;
  ret = pthread_mutex_lock(&(database -> counter_lock));
  if(ret){
    exit_with_error(&ret, "error: lock()\n");
  }
  database -> r_counter++;
  if(database -> r_counter == 1){
      ret = sem_wait(&(database -> w_sem));
      if(-1 == ret){
        exit_with_error(NULL, "error: sem_wait()\n");
      }
  }
  ret = pthread_mutex_unlock(&(database -> counter_lock));
  if(ret){
    exit_with_error(&ret, "error: unlock()\n");
  }
  
  //
  Node* current = database -> head;
  Entry* e = (Entry*)current -> entry;

  while(strcmp(e -> recipient, recipient)){

    current = current -> next;
    if(current == NULL){
      break;
    }
    e = (Entry*)current -> entry;
    
  }
  //
  
  ret = pthread_mutex_lock(&(database -> counter_lock));
  if(ret){
    exit_with_error(&ret, "error: lock()\n");
  }
  database -> r_counter--;
  if(database -> r_counter == 0){
      ret = sem_post(&(database -> w_sem));
      if(-1 == ret){
        exit_with_error(NULL, "error: sem_post()\n");
      }
  } 
  ret = pthread_mutex_unlock(&(database -> counter_lock));
  if(ret){
    exit_with_error(&ret, "error: unlock()\n");
  }

  if(current == NULL){
    return NULL;
  }
  
  //move current at end of database, next time fn is called will return different node
  //now, for this function to complete it also needs to write twice
  //this does not create any concurrency problems as long as every user 
  //works on one connection only
  //might create delays
  current = database_remove(database, current);
  current = database_append_node(database, current);
  return current;
}


void destroy_entry(void* entry, const char* path){
  
  if(entry == NULL) return;
  
  Entry* e = (Entry*)entry;
  mail_database_save_to_disc(e, path);
  free(e -> mail);
  free(e);
}

void destroy_user(void* user, const char* path){//yes, here path is completely vestigial
  if(user == NULL) return;
  User* u = (User*)user;
  free(u);
}

void print_entry(void* entry){
  if(entry == NULL) return;
  
  Entry* e = (Entry*)entry;
  printf("mail: %d, from: %s, to: %s\n", e -> code, e -> sender, e -> recipient);
}

void print_user(void* user){
  if(user == NULL) return;
  User* u = (User*)user;
  printf("%s\n", u -> user_name);
}

void database_destroy(Database* database, void (*destroyer)(void* eu, const char* path), const char* path){
  if(database == NULL || path == NULL){
    return;
  }
  Node* current;
  while(database -> size != 0){
    current = database_pop(database);
    destroyer(current -> entry, path);
    free(current);
  }
  
  int ret;
  ret = sem_destroy(&(database -> w_sem));
  if(-1 == ret){
    exit_with_error(NULL, "error: sem_destroy()\n");
  }
  
  ret = pthread_mutex_destroy(&(database -> counter_lock));
  if(ret){
    exit_with_error(&ret, "error: mutex_destroy()\n");
  }
}

void database_print(Database* database, void (*printer)(void* eu)){
  if(database == NULL){
    return;
  }

  //this is not too important, does not require synchronization
  Node* current = database -> head;
  if(!current){
    printf("empty database\n");
  }
  while(current != NULL){
    printer(current -> entry);
    current = current -> next;
  }
  
}


void mail_database_read_from_disc(Database* database, const char* dir_path){
  if(database == NULL || dir_path == NULL){
    return;
  }
  
  char sender[MAX_USERNAME_SIZE + 1];
  char recipient[MAX_USERNAME_SIZE + 1];
  char file_name[BUFFER_SIZE];
  int id;
  int ret;
  
  DIR *d;
  struct dirent *dir;
  d = opendir(dir_path);
  if (d == NULL){
    exit_with_error(NULL, "error: opendir()");
  }
  
  //find all if any storage files
  while ((dir = readdir(d)) != NULL) {
    memset(sender, 0, MAX_USERNAME_SIZE + 1);
    memset(recipient, 0, MAX_USERNAME_SIZE + 1);
    memset(file_name, 0, BUFFER_SIZE);
  
    char* name = dir->d_name;
    if( !strcmp(name, ".") || !strcmp(name, "..")){
      continue;
    }
    
    memcpy(file_name, dir_path, strlen(dir_path));
    file_name[strlen(dir_path)] = '/';
    strncat(file_name, name, BUFFER_SIZE - strlen(file_name));
    
    //check if file is actually directory
    ret = open(file_name, O_DIRECTORY);
    if(-1 != ret){
      close(ret);
      continue;
    }    
    
    id = atoi(name + 4);
    if(database -> last_id <= id && database -> size != 0){
      database -> last_id = id + 1; //no need for mutex, mutual exclusion is already guaranteed at this state, no process can access yet 
    }
    
    //open a file
    int fd = open(file_name, O_RDWR);  
    if(-1 == fd){
      exit_with_error(NULL, "error: open()\n");
    }
    
    //find how big is file
    size_t file_size = lseek(fd, 0, SEEK_END);
    if(-1 == file_size){
      exit_with_error(NULL,"error: lseek()\n");
    }
  
    ret = lseek(fd, 0, SEEK_SET);
    if(-1 == ret){
      exit_with_error(NULL,"error: lseek()\n");
    }
    
    //map file to treat it as bytes' array
    char* file = (char*)mmap(NULL, file_size, PROT_READ | PROT_WRITE, MAP_PRIVATE, fd, 0);
    if(file == MAP_FAILED){
      exit_with_error(NULL,"error: mmap()\n");
    }
    
    //extracting a node from storage files
    Node* new_n = calloc(1,sizeof(Node));
    if(new_n == NULL){
      exit_with_error(NULL, "error: calloc()\n");
    }
  
    new_n -> next = NULL;
    new_n -> prev = NULL;
  
    new_n -> entry = (void*)malloc(sizeof(Entry));
    if(new_n -> entry == NULL){
      exit_with_error(NULL, "error: malloc()\n");
    }
    
    Entry* new_e = (Entry*) new_n -> entry;
    
    char* token = strtok(file, "$$$");
    memcpy(sender, token, MAX_USERNAME_SIZE + 1);
    
    token = strtok(NULL, "$$$");
    memcpy(recipient, token, MAX_USERNAME_SIZE + 1);
    
    token = strtok(NULL, "$$$");
    int read = atoi(token);
    
    token = strtok(NULL, "$$$"); 
    
    //adding entry to database
    entry_init(database, new_e, token, sender, recipient, id, read); 
    database_append_node(database, new_n);
    
    //cleaning
    ret = munmap(file, file_size);
    if(ret == -1){
      exit_with_error(NULL, "error: munmap()\n");
    }   
    close(fd);   
    unlink(file_name);
  }
  closedir(d);
  database_print(database, print_entry);
  return;
}

void mail_database_save_to_disc(Entry* entry, const char* dir_path){
  if(entry == NULL || dir_path == NULL){
    return;
  } 
  
  int ret;
  char separator[3] = {'$', '$', '$'}; 
  char file_name[BUFFER_SIZE] = {0};
  sprintf(file_name, "%s/mail%d.txt", dir_path, entry -> code);
  
  //creating storage file
  int fd = open(file_name, O_WRONLY | O_CREAT);
  if(fd == -1){
    exit_with_error(NULL, "error: open()\n");
  }
    
  //needs permission if want to re-open
  chmod(file_name, 0600);
    
  //storing entry in file
  ret = write(fd, (void*) entry -> sender, strlen(entry -> sender));
  if(ret == -1){
    exit_with_error(&ret, "error: write()\n");
  }
  
  ret = write(fd, (void*) separator, 3);
  if(ret == -1){
    exit_with_error(&ret, "error: write()\n");
  }
  
  ret = write(fd, (void*) entry -> recipient, strlen(entry -> recipient));
  if(ret == -1){
    exit_with_error(&ret, "error: write()\n");
  }
  
  ret = write(fd, (void*) separator, 3);
  if(ret == -1){
    exit_with_error(&ret, "error: write()\n");
  }

  memset(file_name, 0, BUFFER_SIZE);
  sprintf(file_name, "%d", entry -> read);

  ret = write(fd, (void*) file_name, sizeof(char));
  if(ret == -1){
    exit_with_error(&ret, "error: write()\n");
  }
    
  ret = write(fd, (void*) separator, 3);
  if(ret == -1){
    exit_with_error(&ret, "error: write()\n");
  }
    
  ret = write(fd, (void*) entry -> mail, strlen(entry -> mail));
  if(ret == -1){
    exit_with_error(&ret, "error: write()\n");
  }
  
  ret = write(fd, (void*) separator, 3);
  if(ret == -1){
    exit_with_error(&ret, "error: write()\n");
  }
  
  close(fd);
}

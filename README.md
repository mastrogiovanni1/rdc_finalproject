#MailBox Server

##Description

-the server keeps a database with its mails
-the server can accept more connections from different clients at the same time (multithread)
-the server can handle concurrent access to mail database
-the server will send error messages in case of bad requests such as:
	*messagge won't follow protocol
	*client requests to read a mail not present in database
	*client requests to read a mail not addressed to them
	*client tries to login with username already used by other client

-each client has a name
-each client is alloweed the following requests:
	*read which and how many mails are addressed to them 
	*read one of their mails
	*send a mail to the server
	*delete one of their mails
	*quit session, close connection
	
-each mail has: sender, recipient, identification code
-each database entry has a code, a mail, with the same code, sender of the mail

##Server in detail

The server to run requires the path of a directory where the database is stored when inactive. When the server is run it will load all file in RAM and empty the directory, it is better not to meddle with directory's content.
The server will print on terminal database status and logged users every time it performs an action. 

##Client in detail

Client will ask username to login, in case it is already used by another client it will shutdown.
After client can be given one of five option as described above. Send will also need user to type the path of the mail file to send and username of recipient while read and delete will need user to type number of desired mail, which can be seen via list command. User cannot delete mail they haven't read.

##Protocol in detail

All messages between server an client use the same struct Message. Field ins used to store the instruction to send: LOGIN, LIST, READ, SEND, DELETE, QUIT for client and LISTING, SHOWING, OK, ERROR for server. Field length is used for payload length while payload space can be dinamically allocated when needed. Only SEND and READ messages need payload.

###Interactions

When clients sends LOGIN request, ins = LOGIN, recipient = username. If request is valid server will respond ins = OK else ins = ERROR, length = ErrorCode.

When client sends LIST request, ins = LIST, recipient = username. If request is valid server will respond ins = LISTING, length = mail code, recipient = username, sender = mail sender for every valid mail else ins = ERROR, length = ErrorCode.

When client sends READ request, ins = READ, recipient = username, length = mail code. If request is valid server will respond ins = SHOWING, length = length of mail, payload = mail else ins = ERROR, length = ErrorCode.

When client sends SEND request, ins = SEND, recipient = recipient, sender = username, length = length of mail, payload = mail. If request is valid server will respond ins = OK else ins = ERROR, length = ErrorCode.

When client sends DELETE request, ins = DELETE, length = mail code, recipient = username. If request is valid server will respond ins = OK else ins = ERROR, length = ErrorCode.

When client sends QUIT request, ins = QUIT, recipient = username. If request is valid server will respond ins = OK else ins = ERROR, length = ErrorCode.

If client receives message in wrong format it will assume the server is compromised and it will shutdown.


#include <semaphore.h>
#define MAX_USERNAME_SIZE 15

typedef struct Entry{
  int code;
  int read;
  char sender[MAX_USERNAME_SIZE + 1];
  char recipient[MAX_USERNAME_SIZE + 1];
  char* mail;
}Entry;

typedef struct User{
  char user_name[MAX_USERNAME_SIZE + 1];
}User;

//for mailbox entry points to Entry, for name list points to String
typedef struct Node{
  void* entry;
  struct Node* next;
  struct Node* prev;
}Node;

typedef struct Database{
  Node* head;
  Node* tail;
  int size;
  //implements reader/writer paradigm directly in the structure
  int r_counter;
  sem_t w_sem;
  pthread_mutex_t counter_lock;
  //accessory mutex to assign id to entries
  pthread_mutex_t counter_id;
  //id given to entry
  int last_id;
}Database;

void database_init(Database* database, const char* path);
void entry_init(Database* database, Entry* entry, const char* mail, const char* sender, const char* recipient, int code, int read);

Node* database_append(Database* database, const char* mail, const char* sender, const char* recipient);
Node* database_append_name(Database* database, const char* user_name);
Node* database_append_node(Database* database, Node* n); 

Node* database_get_by_code(Database* database, int code); 
Node* database_get_by_recipient(Database* database, const char* recipient); 
Node* database_get_by_name(Database* database, const char* user_name);

Node* database_pop(Database* database); 
Node* database_remove_by_code(Database* database, int code); 
Node* database_remove_by_name(Database* database, const char* user_name);
Node* database_remove(Database* database, Node* n); 

void mail_database_read_from_disc(Database* database, const char* dir_path);
void mail_database_save_to_disc(Entry* entry, const char* dir_path);

void destroy_entry(void* entry, const char* path);
void destroy_user(void* user, const char* path);
void database_destroy(Database* database, void (*destroyer)(void* eu, const char* path), const char* path); 

void print_entry(void* entry);
void print_user(void* user);
void database_print(Database* database, void (*printer)(void* eu)); 
